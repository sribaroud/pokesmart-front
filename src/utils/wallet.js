import { TezosToolkit, OpKind } from "@taquito/taquito";
import { BeaconWallet } from "@taquito/beacon-wallet";
import config from "../config";

const options = {
    name: "PokeSmart",
    network: {
        type: config.network,
        rpcUrl: config.tezosEndpoint,
    }
};
const wallet = new BeaconWallet(options);

const getActiveAccount = async () => {
    return await wallet.client.getActiveAccount();
};

const connectWallet = async () => {
    let account = await wallet.client.getActiveAccount();

    if (!account) {
        await wallet.requestPermissions();
        account = await wallet.client.getActiveAccount();
    }
    return { success: true, wallet: account.address };
};

const disconnectWallet = async () => {
    await wallet.clearActiveAccount();
    await wallet.disconnect();
    return { success: true, wallet: null };
};

const checkIfWalletConnected = async (wallet) => {
    const activeAccount = await wallet.client.getActiveAccount();
    console.log(activeAccount);
    await wallet.client.requestPermissions()
};

const callSmartContract = async (payload) => {
    await checkIfWalletConnected(wallet);

    const tezos = new TezosToolkit(config.tezosEndpoint);
    tezos.setWalletProvider(wallet);
    const contract = await tezos.wallet.at(config.bridge);
    const operation = await contract.methods.play(payload, config.rollup).send();
    const result = await operation.confirmation();
    console.log(result);
}

const batchCallsSmartContract = async (payloads) => {
    await checkIfWalletConnected(wallet);
    const tezos = new TezosToolkit(config.tezosEndpoint);
    tezos.setWalletProvider(wallet);
    const contract = await tezos.wallet.at(config.bridge);

    const operations = [];
    for (let i = 0; i < payloads.length; i++) {
        operations.push({
            kind: OpKind.TRANSACTION,
            ...contract.methods.play(payloads[i], config.rollup).toTransferParams()
        })
    }

    const batch = tezos.wallet.batch(operations);

    const batchOp = await batch.send();
    await batchOp.confirmation();
}

export {
    connectWallet,
    disconnectWallet,
    getActiveAccount,
    callSmartContract,
    batchCallsSmartContract,
};