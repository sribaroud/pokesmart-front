const config = {
    bridge: "KT1BcdFojoJXWU2af8J24FYxwf9aqKUTvdUt",
    rollup: "sr1FTm9Ldq4Yc3XyDuwUB8FeBeFSFSQHJQQF",
    rollupNodeRpc: "https://evm.ghostnet-evm.tzalpha.net/",
    network: "ghostnet",
    tezosEndpoint: "https://ghostnet.ecadinfra.com",
}

export default config;