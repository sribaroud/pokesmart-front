import { Fragment, SetStateAction } from "react";
import { useState, useEffect, Dispatch } from 'react';

interface Props {
    players: any,
}

function Players({ players }) {

    function goToPlayer(item: { name: string, tz1: string }) {
        console.log('name: ', item.name);
        window.sessionStorage.setItem('player', item.name);
        window.location = window.location.pathname;
    };

    if (!players) return null;

    return (
        <Fragment>
            <h1>Joueurs:</h1>
            < ul className="list-group" >
                {players.map((item: { name: string, tz1: string }) => <li onClick={() => goToPlayer(item)} key={item.tz1} className="list-group-item" >{item.name}</li>)}
            </ul >
        </Fragment >)
}

export default Players;