import { Fragment } from "react";
import { useState, useEffect, useContext } from 'react';
import { useParams } from 'react-router';
import { getInventory, getLastActionResults, getLastGetPoketezDay, getPlayerBalance, getPokedex, bytesToHex, getResults } from "../utils/rpc";
import { batchCallsSmartContract, callSmartContract, getActiveAccount } from "../utils/wallet";
import './Grid.css';
import { pokemonName } from "../utils/card";
import * as rpc from '../utils/rpc';
import { BeaconWallet } from "@taquito/beacon-wallet";
import UpgradeImg from '../images/upgrade.png';
import CoinsImg from '../images/coins.png';
import BuyingBonusImg from '../images/buying_bonus.png';
import CostReductionBonusImg from '../images/cost_reduction.png';
import UltraBallImg from '../images/ultra_ball.png';

interface Props {
    player: string;
    players: any;
}

function colorRarity(rarity) {
    let color = 'black';
    if (rarity == 2) {
        color = 'green';
    }
    if (rarity == 3) {
        color = 'orange';
    }
    if (rarity == 4) {
        color = 'blue';
    }
    if (rarity == 5) {
        color = 'red';
    }
    if (rarity == 6) {
        color = 'gold';
    }
    return color;
}

function idToImage(id) {
    if (id < 152) {
        let url = "https://www.serebii.net/pokemon/art/" + threeDigits(id) + ".png";
        return <img src={url} style={{ width: '150px' }}></img>;
    }
    let url = ""
    if (id >= 152 && id <= 156) {
        if (id == 152) {
            url = "https://www.pokepedia.fr/images/8/83/Pierre_Feu-PGL.png";
        }
        if (id == 153) {
            url = "https://www.pokepedia.fr/images/9/9a/Pierre_Eau-PGL.png";
        }
        if (id == 154) {
            url = "https://www.pokepedia.fr/images/5/55/Pierre_Foudre-PGL.png";
        }
        if (id == 155) {
            url = "https://www.pokepedia.fr/images/2/28/Pierre_Plante-PGL.png";
        }
        if (id == 156) {
            url = "https://www.pokepedia.fr/images/3/30/Pierre_Lune-PGL.png";
        }
        return <img src={url} style={{ width: '90px', height: '90px' }}></img>;
    }
    if (id == 157) {
        url = UpgradeImg;
    }
    if (id == 158) {
        url = UpgradeImg;
    }
    if (id == 159) {
        url = CoinsImg;
    }
    if (id == 160) {
        url = CostReductionBonusImg;
    }
    if (id == 161) {
        url = UltraBallImg;
    }
    if (id == 162) {
        url = BuyingBonusImg;
    }
    return <img src={url} style={{ width: '150px', height: '150px' }}></img>;
}

function evolveBuffer(x, y, z) {
    let x_bytes = intToBytesBE(x);
    let y_bytes = intToBytesBE(y);
    let z_bytes = "";
    if (z) {
        z_bytes = intToBytesBE(z);
    }

    return '0x02' + x_bytes + y_bytes + z_bytes;
}

function upgradeBuffer(cards) {
    let buffer = "";
    for (let i = 0; i < cards.length; i++) {
        buffer += intToBytesBE(cards[i]);
    }

    return '0x05' + buffer;

}

function sellBuffer(cards) {
    let buffer = "";
    for (let i = 0; i < cards.length; i++) {
        buffer += intToBytesBE(cards[i]);
    }

    return '0x06' + buffer;
}

function showPokedex(pokedex) {
    const gridSize = 151;
    const gridElements = [];

    for (let i = 1; i < (gridSize + 1); i++) {
        let src = "https://www.pokebip.com/pages/icones/poke/RB/" + i + ".png"

        const img = () => {
            if (pokedex && pokedex[i - 1].quantity > 0) {
                return <img title="Pokemon" src={src}></img>
            } else {
                return "";
            }
        }
        gridElements.push(<div key={'grid' + i} className="grid-element">{img()}</div >);
    }

    return (
        <div className="grid-container">
            {gridElements}
        </div>
    );
}

function getMew() {
    const get = async () => { await callSmartContract('0x03'); };

    return (
        <button onClick={get}>Récupérer Mew</button>
    )
}

function getPoketez(disabled) {
    const requestPoketez = async () => {
        await callSmartContract('0x00');
    };

    return (
        <button disabled={disabled} onClick={requestPoketez}>Récupérer Poketez</button>
    );
}

function getActionResults(disabled, { buy_cards, explorations }, setRequests) {
    const requestActionResults = async () => {
        let buffer = "";

        // Buy cards
        const bytes = new Uint8Array([(buy_cards >> 8) & 0xFF, buy_cards & 0xFF]);
        const hex = Array.from(bytes, function (byte) {
            return ('0' + (byte & 0xFF).toString(16)).slice(-2);
        }).join('');
        buffer += hex;

        // Explorations
        let cards = Array.from(explorations);
        for (let i = 0; i < cards.length; i++) {
            buffer += intToBytesBE(cards[i]);
        }

        await callSmartContract('0x01' + buffer);
        setRequests({ buy_cards: 0, explorations: new Set() });
    };

    return (
        <button disabled={disabled} onClick={requestActionResults}>Déclencher action(s)</button>
    );
}

function buyCards(n, setN, requests, setRequests) {
    const handleNumberChanger = (event) => {
        const value = parseInt(event.target.value);
        setN(value);
    }

    const buy = async () => {
        let new_requests = { buy_cards: requests.buy_cards + n, explorations: requests.explorations };
        setRequests(new_requests);
    }

    return (
        <div>
            <input
                type="number"
                value={n}
                onChange={handleNumberChanger}
                placeholder="0"
            />
            <button onClick={buy}>Acheter des cartes</button>
        </div>
    );
}

function showActionRequests(
    { buy_cards, explorations },
    setRequests,
    selectedCells,
    setSelectedCells,
    bonus,
    balance,
    inventory
) {
    if (buy_cards == 0 && explorations.size == 0) {
        return "";
    }

    let reduction = 0;
    if (bonus) {
        reduction = bonus.stat_cards / 100;
    }

    let cost_per_card = 2000 - 2000 * reduction;
    let cost_per_explo = 3000 - 3000 * reduction;
    let total_cost = cost_per_card * buy_cards + cost_per_explo * explorations.size;

    const cancelBuy = async () => {
        let new_requests = { buy_cards: 0, explorations };
        setRequests(new_requests);
    }

    const handleCellClick = (event, uuid) => {
        event.preventDefault();
        if (selectedCells.has(uuid)) {
            selectedCells.delete(uuid);
        } else {
            selectedCells.add(uuid);
        }
        setSelectedCells(new Set(selectedCells));
    }


    const cancelExplore = async () => {
        let new_explorations = new Set([...explorations]);
        for (let to_cancel of selectedCells) {
            new_explorations.delete(to_cancel);
        }
        let new_requests = { buy_cards, explorations: new_explorations };
        setRequests(new_requests);
        console.log(new_requests);
        setSelectedCells(new Set());
        console.log('cancelExplore');
    };

    let cost_style = { color: 'black' };
    if (total_cost > balance) {
        cost_style = { color: 'red' }
    }

    let full_explorations = []
    for (let element of explorations) {
        inventory.forEach(card => {
            if (card.uuid == element) {
                full_explorations.push(card);
            }
        });
    }

    return (
        <div>
            <span>Prochaine action(s), coût prévu:</span><span style={cost_style}>{total_cost}</span>
            <table>
                <tbody>
                    <tr>
                        {(buy_cards > 0) ? (<td style={{
                            border: '1px solid black',
                            width: '180px',
                            textAlign: 'center',
                            verticalAlign: 'center'
                        }}> Achat de {buy_cards} carte(s)</td>) : null}
                        {full_explorations.map((item) => (
                            <td
                                onClick={(e) => handleCellClick(e, item.uuid)}
                                style={{
                                    border: '1px solid black',
                                    width: '180px',
                                    textAlign: 'center',
                                    verticalAlign: 'center',
                                    background: selectedCells.has(item.uuid) ? 'yellow' : 'inherit',
                                }}>
                                <span>Exploration</span>
                                {idToImage(item.id)}
                                <span style={{ fontWeight: 'bold' }}>{item.name}</span>
                                <br></br>
                                <span>{item.stat_rarity} en rareté</span>
                                <br></br>
                                <span>{item.stat_cards} en cartes</span>
                            </td>
                        ))}
                    </tr>
                </tbody>
            </table>
            <button onClick={cancelBuy}>Annuler achat</button>
            <button onClick={cancelExplore}>Annuler exploration</button>
        </div>
    )
}

function threeDigits(number) {
    // Convert the number to a string
    const numberStr = number.toString();
    // Add leading zeros to make it at least three digits
    const paddedNumberStr = '00' + numberStr;
    // Get the last three characters
    const result = paddedNumberStr.slice(-3);
    return result;
}

function intToBytesBE(integer) {
    const byteArray = new Array(4);
    byteArray[0] = (integer >> 24) & 0xff;
    byteArray[1] = (integer >> 16) & 0xff;
    byteArray[2] = (integer >> 8) & 0xff;
    byteArray[3] = integer & 0xff;
    return bytesToHex(byteArray);
}

function showResults(rawResults, isHidden, setIsHidden, showRecap) {

    const displayLine = (result) => {

        const displayBuy = (result) => {
            if (result.length == 1) {
                return "";
            }
            return (<td key={"buy"} style={{
                border: '1px solid black',
                width: '180px',
                textAlign: 'center',
                verticalAlign: 'center'
            }}>Achat de {result.length - 1} cartes</td>)
        };

        const displayExplo = (result) => {
            let item = result[0];
            return (<td style={{
                border: '1px solid black',
                width: '180px',
                textAlign: 'center',
                verticalAlign: 'center',
            }}>
                <span>Exploration</span>
                {showCardData(item)}
            </td>);
        };

        return (
            <tr key={'line' + result[0].uuid}>
                {result[0].stat_rarity == 0 ? displayBuy(result) : displayExplo(result)}
                {result.map((item, index) => {
                    if (index == 0) {
                        return "";
                    }
                    return (
                        <td key={'explo' + item.uuid} style={{
                            border: '1px solid black',
                            width: '180px',
                            textAlign: 'center',
                            verticalAlign: 'center',
                        }}>{showCardData(item)}
                        </td>);
                })}
            </tr >
        );
    }

    if (rawResults.length == 0) {
        return "";
    };

    const handleClick = async () => {
        setIsHidden(!isHidden);
    };

    return (
        <div>
            {showRecap ? (<span onClick={handleClick}>Résultats de la dernière action (appuyer pour cacher/montrer)</span>) : null}
            {isHidden ? null : (
                <table>
                    <tbody>
                        {rawResults.map((item) => displayLine(item))}
                    </tbody>
                </table>)
            }
        </div >
    )
}

function stringToHex(input) {
    let hex = '';
    for (let i = 0; i < input.length; i++) {
        const charCode = input.charCodeAt(i).toString(16);
        hex += charCode;
    }
    return hex;
}

function showCardData(item) {
    const showPokemon = (item) => {

        let name = item.name;
        if (name == null) {
            name = pokemonName(item.id);
        }

        if (item.id == 151) {
            item.rarity = 6;
        }

        return (
            <div>
                {idToImage(item.id)}
                <br></br >
                <span style={{ fontWeight: 'bold' }}>{name}</span>
                <br></br>
                {item.rarity == 0 ? null : (<div><span style={{ color: colorRarity(item.rarity) }}>{'★'.repeat(item.rarity)}</span><br></br></div>)}
                <span>{item.stat_rarity} en rareté</span>
                <br></br>
                <span>{item.stat_cards} en cartes</span>
            </div>
        );
    }

    const showPierre = (item) => {
        return (
            <div>
                {idToImage(item.id)}
                <br></br >
                <br></br>
                <span style={{ fontWeight: 'bold' }}>{pokemonName(item.id)}</span>
                <br></br>
                <span style={{ color: colorRarity(item.rarity) }}>{'★'.repeat(item.rarity)}</span>
            </div>
        );
    }

    const showUpgrade = (item) => {
        return (
            <div>
                {idToImage(item.id)}
                <br></br>
                <span style={{ color: colorRarity(item.rarity) }}>{'★'.repeat(item.rarity)}</span>
                <br></br>
                <span>+{item.id == 157 ? item.stat_rarity : item.stat_cards} en {item.id == 157 ? "rareté" : "cartes"}</span>
            </div>
        );
    }

    const showPoketez = (item) => {
        return (
            <div>
                {idToImage(item.id)}
                <br></br>
                <span style={{ color: colorRarity(item.rarity) }}>{'★'.repeat(item.rarity)}</span>
                <br></br>
                <br></br>
                <span>{item.stat_rarity} pꜩ</span>
            </div>
        );
    }

    const showBonus = (item) => {
        let rarity, cost;
        if (item.id == 160 || item.id == 162) {
            cost = "-" + item.stat_cards + "% aux coûts";
        }
        if (item.id == 161 || item.id == 162) {
            rarity = item.stat_rarity + " en rareté à l'achat";
        }

        return (
            <div>
                {idToImage(item.id)}
                <br></br>
                <span style={{ fontWeight: 'bold' }}>{pokemonName(item.id)}</span>
                <br></br>
                <span style={{ color: colorRarity(item.rarity) }}>{'★'.repeat(item.rarity)}</span>
                <br></br>
                <span>{rarity}</span>
                <br></br>
                <span>{cost}</span>
            </div>
        );
    }

    if (item.id <= 151) {
        return showPokemon(item);
    }

    if (item.id >= 152 && item.id <= 156) {
        return showPierre(item);
    }

    if (item.id == 157 || item.id == 158) {
        return showUpgrade(item);
    }

    if (item.id == 159) {
        return showPoketez(item);
    }

    if (item.id >= 160 && item.id <= 162) {
        return showBonus(item);
    }

    return "";
}

function getCard(inventory, uuid) {
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].uuid == uuid) {
            return inventory[i];
        }
    }
    return null;
}

function showBatcherOperations(
    inventory,
    batcherOperations,
    setBatcherOperations,
    usedCards,
    setUsedCards
) {
    if (batcherOperations.size == 0) {
        return "";
    }

    let style = {
        border: '1px solid black',
        width: '180px',
        textAlign: 'center',
        verticalAlign: 'center'
    };

    const showOperation = (operation) => {
        const elements = [];
        if (operation.type == 'Evolution') {
            elements.push(<td key={'evolution' + operation.x} style={style}>{showCardData(getCard(inventory, operation.x))}</td>);
            elements.push(<td key={'evolution' + operation.y} style={style}>{showCardData(getCard(inventory, operation.y))}</td>);
            if (operation.z) {
                elements.push(<td key={'evolution' + operation.z} style={style}>{showCardData(getCard(inventory, operation.z))}</td>);
            }
        }
        if (operation.type == 'Amélioration') {
            for (let i = 0; i < operation.cards.length; i++) {
                elements.push(<td key={'upgrade' + operation.cards[i]} style={style}>{showCardData(getCard(inventory, operation.cards[i]))}</td>);
            }
        }
        if (operation.type == 'Vendre') {
            for (let i = 0; i < operation.cards.length; i++) {
                elements.push(<td key={'sell' + operation.cards[i]} style={style}>{showCardData(getCard(inventory, operation.cards[i]))}</td>);
            }
        }
        return elements;
    }

    const withdraw = async (operation) => {
        let newUsedCards = new Set([...usedCards]);
        let newOperations = new Set([...batcherOperations]);
        if (operation.type == 'Evolution') {
            newUsedCards.delete(operation.x);
            newUsedCards.delete(operation.y);
            if (operation.z) {
                newUsedCards.delete(operation.z);
            }
        }
        if (operation.type == 'Amélioration' || operation.type == 'Vendre') {
            for (let i = 0; i < operation.cards.length; i++) {
                newUsedCards.delete(operation.cards[i]);
            }
        }
        newOperations.delete(operation);
        setUsedCards(newUsedCards);
        setBatcherOperations(newOperations);
    }

    let data = [];
    let values = Array.from(batcherOperations);
    for (let i = 0; i < values.length; i++) {
        const row = (
            <tr>
                <td key={'operations' + i} style={style}>
                    <span>{values[i].type}</span>
                    <br></br><br></br>
                    <button onClick={() => withdraw(values[i])}>Retirer</button>
                </td>
                {showOperation(values[i])}
            </tr>
        );
        data.push(row);
    }

    const applyOperations = async () => {
        let payloads = [];
        let operations = Array.from(batcherOperations);
        for (let i = 0; i < operations.length; i++) {
            let operation = operations[i];
            if (operation.type == 'Evolution') {
                payloads.push(evolveBuffer(operation.x, operation.y, operation.z))
            }
            if (operation.type == 'Amélioration') {
                payloads.push(upgradeBuffer(operation.cards));
            }
            if (operation.type == 'Vendre') {
                payloads.push(sellBuffer(operation.cards));
            }
        }
        await batchCallsSmartContract(payloads);
        setBatcherOperations(new Set());
        setUsedCards(new Set());
    }

    return (<div>
        <br></br>
        <span>Opérations en préparation:</span>
        <div>
            <table>
                <tbody>
                    {data}
                </tbody>
            </table>
        </div>
        <button style={buttonStyle} onClick={applyOperations}>Appliquer les opérations</button>
        <br></br>
    </div>)
}

function splitInventory(inventory) {
    let bonuses = [];
    let pokemons = [];
    let stones = [];
    let upgrades = [];
    let poketez = [];
    for (let i = 0; i < inventory.length; i++) {
        let card = inventory[i];
        if (card.id <= 151) {
            pokemons.push(card);
        }
        if (card.id >= 152 && card.id <= 156) {
            stones.push(card);
        }
        if (card.id == 157 || card.id == 158) {
            upgrades.push(card);
        }
        if (card.id == 159) {
            poketez.push(card);
        }
        if (card.id >= 160 && card.id <= 162) {
            bonuses.push(card);
        }
    }
    return { bonuses, pokemons, stones, upgrades, poketez };
}


function showInventory(
    selectedCells,
    setSelectedCells,
    inventory,
    batcherOperations,
    setBatcherOperations,
    usedCards,
    setUsedCards,
    sortStyle,
    setSortStyle,
) {

    const handleCellClick = (event, uuid) => {
        event.preventDefault();
        if (usedCards.has(uuid)) {
            return;
        }
        if (selectedCells.has(uuid)) {
            selectedCells.delete(uuid);
        } else {
            selectedCells.add(uuid);
        }
        setSelectedCells(new Set(selectedCells));
    }

    const backgroundColor = (uuid) => {
        if (usedCards.has(uuid)) {
            return '#DCDCDC';
        }
        if (selectedCells.has(uuid)) {
            return 'yellow';
        }
        return 'inherit';
    }

    function splitInRows(name, cards) {
        if (cards.length == 0) {
            return "";
        }

        let data = [];
        for (let i = 0; i < cards.length; i += 10) {
            const elements = cards.slice(i, i + 10);
            const row = (
                <tr key={'data' + i}>
                    {elements.map((item) => (
                        <td key={'element' + item.uuid} onClick={(e) => handleCellClick(e, item.uuid)} style={{
                            border: '1px solid black',
                            width: '180px',
                            textAlign: 'center',
                            verticalAlign: 'center',
                            background: backgroundColor(item.uuid),
                        }}>{showCardData(item)}
                        </td>
                    ))
                    }
                </tr >
            );
            data.push(row);
        }
        return (
            <div>
                <span>{name}:</span>
                <table>
                    <tbody>
                        {data}
                    </tbody>
                </table>
            </div>
        );
    }


    const handleSortChange = (e) => {
        setSortStyle(e.target.value);
    }

    let sort_fn = (a, b) => {
        return a.id - b.id;
    };
    if (sortStyle == 'Rareté') {
        sort_fn = (a, b) => {
            return b.rarity - a.rarity;
        }
    }
    if (sortStyle == 'Statistique rareté') {
        sort_fn = (a, b) => {
            return b.stat_rarity - a.stat_rarity;
        }
    }
    if (sortStyle == 'Statistique cartes') {
        sort_fn = (a, b) => {
            return b.stat_cards - a.stat_cards;
        }
    }

    if (sortStyle == 'Statistique somme') {
        sort_fn = (a, b) => {
            return (b.stat_rarity + b.stat_cards) - (a.stat_rarity + a.stat_cards);
        }
    }

    let sorted_inventory = inventory.slice().sort(sort_fn);
    let { bonuses, pokemons, upgrades, stones, poketez } = splitInventory(sorted_inventory);

    return (
        <div>
            <br></br><br></br>
            <span>Trier par: </span>
            <select style={{
                backgroundColor: 'inherit',
                color: 'black',
                border: '1px solid black',
                borderTadius: '4px',
                padding: '10px 20px',
                marginLeft: '0px',
                marginRight: '10px',
                cursor: 'pointer',
                height: '46px',
            }} value={sortStyle} onChange={handleSortChange}>
                <option>Classique</option>
                <option>Rareté</option>
                <option>Statistique rareté</option>
                <option>Statistique cartes</option>
                <option>Statistique somme</option>
            </select>
            <br></br>
            <br></br>
            {splitInRows("Inventaire des bonus (non actifs)", bonuses)}
            {splitInRows("Pokemons", pokemons)}
            {splitInRows("Pierres", stones)}
            {splitInRows("Améliorations", upgrades)}
            {splitInRows("Poketez", poketez)}

            {showBatcherOperations(
                inventory,
                batcherOperations,
                setBatcherOperations,
                usedCards,
                setUsedCards
            )}

        </div >
    )
}

const buttonStyle = {
    backgroundColor: 'inherit',
    color: 'black',
    border: '1px solid black',
    borderTadius: '4px',
    padding: '10px 20px',
    margin: '0 10px',
    cursor: 'pointer',
}

function showActionButtons(
    selectedPlayer,
    setSelectedPlayer,
    selectedCells,
    setSelectedCells,
    players,
    requests,
    setRequests,
    batcher,
    setBatcher,
    batcherOperations,
    setBatcherOperations,
    usedCards,
    setUsedCards,
) {

    const evolve = async () => {
        let values = Array.from(selectedCells);
        let n = values.length;
        if (n < 2 || n > 3) {
            console.log('Invalid evolve payload');
            return;
        }

        let x = values[0];
        let y = values[1];
        let z = values[2];

        if (batcher) {
            let op = {
                type: 'Evolution',
                x, y, z,
            };
            batcherOperations.add(op);
            setBatcherOperations(batcherOperations);
            usedCards.add(x);
            usedCards.add(y);
            if (z) {
                usedCards.add(z);
            }
            setUsedCards(usedCards);
        } else {
            let buffer = evolveBuffer(x, y, z);
            await callSmartContract(buffer);
        }
        setSelectedCells(new Set());
    };

    const explore = async () => {
        let explorations = new Set([...requests.explorations, ...selectedCells]);
        let new_requests = { buy_cards: requests.buy_cards, explorations };
        for (let card of selectedCells) {
            usedCards.add(card);
        }
        setRequests(new_requests);
        setSelectedCells(new Set());
    }

    const upgrade = async () => {
        let cards = Array.from(selectedCells);

        if (batcher) {
            let op = {
                type: 'Amélioration',
                cards
            }
            batcherOperations.add(op);
            setBatcherOperations(batcherOperations);
            for (let card of cards) {
                usedCards.add(card);
            }
            setUsedCards(usedCards);
        } else {
            let buffer = upgradeBuffer(cards);
            await callSmartContract(buffer);
        }
        setSelectedCells(new Set());
    }

    const sell = async () => {
        let cards = Array.from(selectedCells);

        if (batcher) {
            let op = {
                type: 'Vendre',
                cards
            }
            batcherOperations.add(op);
            setBatcherOperations(batcherOperations);
            for (let card of cards) {
                usedCards.add(card);
            }
            setUsedCards(usedCards);
        } else {
            let buffer = sellBuffer(cards);
            await callSmartContract(buffer);
        }
        setSelectedCells(new Set());
    }

    const setBonus = async () => {
        let cards = Array.from(selectedCells);
        if (cards.length != 1) {
            console.log("Invalid items selected for set bonus.");
            setSelectedCells(new Set());
        } else {
            let bonus = intToBytesBE(cards[0]);
            await callSmartContract('0x07' + bonus);
        }
    }

    function displaySendExplorationsButton() {
        return (
            <button style={buttonStyle} onClick={explore}>Envoyer en exploration</button>
        );
    }

    function displayUpgradeButton() {
        return (
            <button style={buttonStyle} onClick={upgrade}>Améliorer</button>
        );
    }

    function displaySellButton() {
        return (
            <button style={buttonStyle} onClick={sell}>Vendre</button>
        );
    }

    function displaySetBonus() {
        return (
            <button style={buttonStyle} onClick={setBonus}>Placer bonus</button>
        );
    }

    const handlePlayerChange = (e) => {
        setSelectedPlayer(e.target.value);
    };

    const transfer = async () => {
        let to = stringToHex(window.sessionStorage.getItem(selectedPlayer));
        let cards = Array.from(selectedCells);
        let buffer = "";
        for (let i = 0; i < cards.length; i++) {
            buffer += intToBytesBE(cards[i]);
        }
        await callSmartContract('0x04' + to + buffer);
        setSelectedCells(new Set());
    }

    const batcherMode = async () => {
        setBatcher(!batcher);
    }

    let otherPlayers = players;

    return (
        <footer style={{
            backgroundColor: 'rgba(255, 255, 255, 0.8)',
            color: '#fff',
            textAlign: 'center',
            padding: '10px',
            position: 'fixed',
            bottom: '0',
            width: '100%',
        }}>
            <button style={{
                backgroundColor: batcher ? '#32de84' : 'inherit',
                color: 'black',
                border: '1px solid black',
                borderTadius: '4px',
                padding: '10px 20px',
                cursor: 'pointer',
                marginRight: '20px',
            }} onClick={batcherMode}>Mode batcher</button>
            {(!batcher) ? (<button style={{
                backgroundColor: 'inherit',
                color: 'black',
                border: '1px solid black',
                borderTadius: '4px',
                padding: '10px 20px',
                cursor: 'pointer',
            }} onClick={transfer}>Envoyer à</button>) : null}
            {(!batcher) ? (<select style={{
                backgroundColor: 'inherit',
                color: 'black',
                border: '1px solid black',
                borderTadius: '4px',
                padding: '10px 20px',
                marginLeft: '0px',
                marginRight: '10px',
                cursor: 'pointer',
                height: '46px',
            }} value={selectedPlayer} onChange={handlePlayerChange}>
                {otherPlayers.map((item) => (
                    <option key={item.tz1} value={item.name}>{item.name}</option>
                ))}
            </select>) : null}
            <button style={buttonStyle} onClick={evolve}>Evoluer</button>
            {(!batcher) ? displaySendExplorationsButton() : null}
            {displayUpgradeButton()}
            {displaySellButton()}
            {(!batcher) ? displaySetBonus() : null}
            <br></br>
        </footer >
    );
}

function showBonus(bonus, isPlayer) {

    if (!bonus) {
        return "";
    }

    const unset = async () => {
        await callSmartContract('0x08');
    }

    function showUnset() {
        return (
            <div>
                <button style={buttonStyle} onClick={unset}>Retirer</button>
                <br></br>
                <br></br>
            </div>
        );
    }

    return (
        <div>
            <br></br>
            <span>Bonus:</span>
            <div style={{
                border: '1px solid black',
                width: '180px',
                textAlign: 'center',
                verticalAlign: 'center'
            }}>
                {showCardData(bonus)}
            </div>
            <br></br>
            {isPlayer ? showUnset() : null}
        </div >
    )
}

function isPokedexFull(pokedex) {
    for (let i = 0; i < 150; i++) {
        if (pokedex[i].quantity == 0) {
            return false;
        }
    }
    return true;
}

function Player({ player, players }) {
    const [balance, setBalance] = useState(0);
    const [account, setAccount] = useState(null);
    const [pokedex, setPokedex] = useState(null);
    const [poketezDisabled, setPoketezDisabled] = useState(true);
    const [actionDisabled, setActionDisabled] = useState(true);
    const [lastGetPoketez, setLastGetPoketez] = useState(true);
    const [lastAction, setLastAction] = useState(true);
    const [buyN, setBuyN] = useState(0);
    const [requests, setRequests] = useState({ buy_cards: 0, explorations: new Set() });
    let [rawResults, setResults] = useState([]);
    let [hideResults, setHideResults] = useState(false);
    let [rawInventory, setInventory] = useState([]);
    let [selectedCells, setSelectedCells] = useState(new Set());
    const [selectedPlayer, setSelectedPlayer] = useState(null);
    const [loading, setLoading] = useState(true);
    const [isButtonsVisible, setIsButtonsVisible] = useState(false);
    const [bonus, setBonus] = useState(null);
    const [pokedexFull, setPokedexFull] = useState(false);
    const [batcher, setBatcher] = useState(false);
    const [batcherOperations, setBatcherOperations] = useState(new Set());
    const [usedCards, setUsedCards] = useState(new Set());
    const [sortStyle, setSortStyle] = useState('Classique');

    let tz1 = null;
    if (!tz1) {
        tz1 = window.sessionStorage.getItem(player);
    }

    useEffect(() => {
        (async () => {
            // Get Pokedex
            let pokedex = await getPokedex(tz1);
            setPokedex(pokedex);
            if (isPokedexFull(pokedex)) {
                setPokedexFull(true);
            }
            // Get balance
            let balance = await getPlayerBalance(tz1);
            setBalance(balance);
            // Get current viewer's account
            let x = await getActiveAccount();
            if (x) {
                setAccount(x.address);
            }

            let now = Date.now();
            let seconds = now / 1000;
            let t = Math.floor(seconds / 86400);

            // Get last get poketez day
            let poketez_last_ms = await getLastGetPoketezDay(tz1) * 1000;
            setLastGetPoketez(poketez_last_ms);
            let poketez_last = Math.floor((poketez_last_ms / 1000) / 86400);

            if (t > poketez_last) {
                setPoketezDisabled(false);
            } else {
                setPoketezDisabled(true);
            }
            // Get last action
            let action_last_ms = await getLastActionResults(tz1) * 1000;
            setLastAction(action_last_ms);
            let action_last = Math.floor((action_last_ms / 1000) / 86400);

            if (t > action_last) {
                setActionDisabled(false);
            } else {
                setActionDisabled(true);
            }
            // Get results
            let rawResults = await getResults(tz1);
            setResults(rawResults);
            // Get inventory
            let rawInventory = await getInventory(tz1);
            setInventory(rawInventory);
            if (!selectedPlayer) {
                setSelectedPlayer(players[0].name);
            }
            // Get account
            let v = await getActiveAccount();
            if (v) {
                setAccount(v.address);
            }
            // Get bonus
            let bonus = await rpc.getBonus(tz1);
            setBonus(bonus);

            // Loading finished
            setLoading(false);
        })();
    }, []);

    if (loading) {
        return <span>Loading..</span>;
    }

    let inventory = rawInventory.map(({ id, uuid, rarity, stat_rarity, stat_cards }) => {
        const name = pokemonName(id);
        return { uuid, rarity, id, name, stat_rarity, stat_cards }
    })

    let isPlayer = account && (account == tz1);
    return (
        <Fragment>
            <br></br>
            <span style={{ fontWeight: 'bold' }}>{player}</span>
            <span>: </span>
            <span>{tz1}</span>
            <br></br>
            <p>{balance} pꜩ</p>
            {(lastGetPoketez > 0) ? (<p>Dernière récupération de poketez: {new Date(lastGetPoketez).toLocaleDateString()}</p>) : null}
            {(lastAction > 0) ? (<p>Dernière action: {new Date(lastAction).toLocaleDateString()}</p>) : null}
            {(isPlayer && pokedexFull) ? getMew() : null}
            {isPlayer ? getPoketez(poketezDisabled) : null}
            {isPlayer ? getActionResults(actionDisabled, requests, setRequests) : null}
            {isPlayer ? buyCards(buyN, setBuyN, requests, setRequests) : null}
            <br></br>
            {showActionRequests(
                requests,
                setRequests,
                selectedCells,
                setSelectedCells,
                bonus,
                balance,
                inventory
            )}
            <br></br>
            {showPokedex(pokedex)}
            <br></br>
            {showBonus(bonus, isPlayer)}
            {showResults(rawResults, hideResults, setHideResults, true)}
            {showInventory(
                selectedCells,
                setSelectedCells,
                rawInventory,
                batcherOperations,
                setBatcherOperations,
                usedCards,
                setUsedCards,
                sortStyle,
                setSortStyle,
            )}
            <br></br><br></br><br></br>
            {isPlayer ? showActionButtons(
                selectedPlayer,
                setSelectedPlayer,
                selectedCells,
                setSelectedCells,
                players,
                requests,
                setRequests,
                batcher,
                setBatcher,
                batcherOperations,
                setBatcherOperations,
                usedCards,
                setUsedCards,
            ) : null}
        </Fragment>
    )
}

export default Player;
export {
    showResults
}