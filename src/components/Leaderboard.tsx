
import { useState, useEffect, useContext } from 'react';
import * as rpc from '../utils/rpc';
import { pokemonName } from '../utils/card';

function sumOfPokedex(pokedex) {
    let amount = 0;
    for (let i = 0; i < pokedex.length; i++) {
        amount += pokedex[i].quantity;
    }
    return amount;
}

function missingCards(pokedex) {
    let amount = 0;

    for (let i = 0; i < pokedex.length; i++) {
        if (pokedex[i].quantity > 0) {
            amount += 1
        }
    }
    return (151 - amount);
}

function Leaderboard(objectPlayers) {

    let [pokedexs, setPokedexs] = useState([]);
    let [loading, setLoading] = useState(true);

    let players = objectPlayers.players;

    useEffect(() => {
        (async () => {
            let pokedexs = await Promise.all(players.map(async player => {
                let pokedex = await rpc.getPokedex(player.tz1);
                return pokedex;
            }))
            setLoading(false);
            setPokedexs(pokedexs);
        })();
    }, []);

    if (loading) {
        return "Loading..";
    }

    let leaderBoard = players.map((item, index) => {
        let pokedex = pokedexs[index];
        let cards = sumOfPokedex(pokedex);
        let missing = missingCards(pokedex);
        return { name: item.name, pokedex, cards, missing }
    });


    leaderBoard = leaderBoard.slice().sort((a, b) => a.missing - b.missing);

    let tdStyle = {
        border: '1px solid black',
        borderCollapse: 'collapse',
        cellPadding: '5px',
        alignContent: 'center',
        padding: '5px 5px',
        width: '50px',
        height: '15px',
    };

    return (
        <div>
            <br></br>
            <table style={{
                border: '1px solid black',
                borderCollapse: 'collapse',
                textAlign: 'center',
                verticalAlign: 'center',
                marginLeft: '10px',
            }}>
                <tbody>
                    <tr>
                        <td style={tdStyle}></td>
                        {leaderBoard.map((item) => {
                            return <td style={tdStyle}><a href={'?name=' + item.name}>{item.name}</a></td>
                        })}
                    </tr>
                    <tr>
                        <td style={tdStyle}>Cartes</td>
                        {leaderBoard.map((item) => {
                            return <td style={tdStyle}>{item.cards}</td>;
                        })}
                    </tr>
                    <tr>
                        <td style={tdStyle}>Cartes manquantes</td>
                        {leaderBoard.map((item) => {
                            return <td style={tdStyle}>{item.missing}</td>;
                        })}
                    </tr>
                    {leaderBoard[0].pokedex.map((_item, index) => {
                        return (<tr>
                            <td style={tdStyle}>{pokemonName(index + 1)}</td>
                            {leaderBoard.map((item) => {
                                return <td style={tdStyle}>{item.pokedex[index].quantity}</td>
                            })}
                        </tr>)
                    })}
                </tbody>
            </table>
        </div >
    );
}

export default Leaderboard;